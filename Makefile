CC=gcc
CFLAGS=-std=c99 -Wall


# additional flags for gcov
TESTFLAGS=-fprofile-arcs -ftest-coverage

clean:
	rm -f *.o testhasard testcomparaison testapp *.gcov *.gcda *.gcno

testcomparaison: testcomparaison.c comparaison.h comparaison.c
        # build the comparaison test
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testcomparaison.c comparaison.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testcomparaison testcomparaison.o comparaison.o
        
        # run the test, which will generate testcomparaison.gcna and testcompa>
	./testcomparaison

        # compute how test is covering testcomparaison.c
	gcov -c -p testcomparaison 

testhasard: testhasard.c hasard.h hasard.c
        # build the hasard test
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testhasard.c hasard.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testhasard testhasard.o hasard.o

        # run the hasard test, which will generate testhasard.gcna and testhas
	./testhasard

        # compute how test is covering testhasard.c
	gcov -c -p testhasard

testapp: testapp.c hasard.h comparaison.h hasard.c comparaison.c
        # build the hasard test
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testapp.c hasard.c comparaison.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testapp testapp.o hasard.o comparaison.o

        # run the hasard test, which will generate testhasard.gcna and testhasard.gcno
	./testapp R P C R R C C P P

        # compute how test is covering testhasard.c
	gcov -c -p testapp


